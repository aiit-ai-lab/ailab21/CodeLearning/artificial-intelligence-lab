//
// Created by 江北 on 2022/10/12.
//
#include <iostream>
#include <string>
#include<algorithm>
using namespace  std;
string AddString(string a,string b);

string AddString(string a,string b){
    int la=a.size()-1;
    int lb=b.size()-1;
    string res;
    int carry=0;
    while(la>=0&&lb>=0){
        int num=carry+a[la]-'0'+b[lb]-'0';
        res+=num%10+'0';
        carry=num/10;
        la--;
        lb--;
    }
    while(la>=0){
        int num=carry+a[la]-'0';
        res+=num%10+'0';
        carry=num/10;
        la--;
    }
    while(lb>=0){
        int num=carry+b[lb]-'0';
        res+=num%10+'0';
        carry=num/10;
        lb--;
    }
    if(carry>0){
        res+=to_string(carry);
    }
    reverse(res.begin(),res.end());
    return res;
}
int main(){
    string a,b;
    while(cin>>a>>b) {
        cout << AddString(a, b) << endl;
    }
    return 0;
}
