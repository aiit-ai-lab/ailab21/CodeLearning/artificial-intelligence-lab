#include <iostream>
using namespace std;
int temp=0;

void ping(int x,int y){
    if(x<4&&y<3){
        cout<<temp<<endl;
        return ;
    }
    int sum=0;
    if(x>=4){
        temp+=x/4;
        sum+=x/4;
        x=x%4;
    }
    if(y>=3){
        temp+=y/3;
        sum+=y/3;
        y=y%3;
    }
    ping(x+sum,y+sum);
}
int main(){
    int T;
    cin>>T;
    while(T--){
        int n;
        cin>>n;
        temp=n;
        ping(n,n);
    }
    return 0;
}
